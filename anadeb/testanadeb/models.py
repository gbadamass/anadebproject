from django.db import models

# Create your models here.
# LE MODEL DE NOTRE CAPSULE
class Capsules(models.Model):
      nom = models.CharField(max_length=200)
      description = models.CharField(max_length=400)
      numerovol = models.FloatField()
      auto = models.BooleanField(default=False)

      def __str__(self):
            return  self.nom
