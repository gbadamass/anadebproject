from django.contrib import admin
from .models import Capsules

class CapsulesAdmin(admin.ModelAdmin):
    list_display = ('nom', 'description', 'numerovol', 'auto')
    # OPTION DE RECHERCHE
    search_fields = ['nom']
# Register your models here.
admin.site.register(Capsules, CapsulesAdmin)