from django.shortcuts import render
from django.http import HttpResponse
from .models import Capsules

# Create your views here.
# / testanadeb
def index(request):
    '''capsules = Capsules.objects.all()
    capsules_names_and_numeros =[capsules.nom + " : " + str(capsules.numerovol) + "" for capsules in capsules]
    capsules_names_and_numeros_str = ", ".join(capsules_names_and_numeros)
    return HttpResponse("La liste des Capsules :" + capsules_names_and_numeros_str)'''
    capsules = Capsules.objects.all().order_by('numerovol')
    return render(request, 'testanadeb/index.html', {'capsules':capsules})