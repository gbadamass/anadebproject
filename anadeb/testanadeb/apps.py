from django.apps import AppConfig


class TestanadebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'testanadeb'
